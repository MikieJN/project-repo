window.onload = function() {
	getSessionTickets();
}

function getSessionTickets() {
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			let reimbursement = JSON.parse(xhttp.responseText);
			console.log(reimbursement);
			for (let i = 1; i <= reimbursement.length; i++) {
				let tr = document.getElementById("table-body").appendChild(document.createElement("tr"));
				tr.setAttribute("id", `row${i}`);
			}
			let i = 1;
			for (reimb of reimbursement) {
				let tr = document.getElementById(`row${i}`);
				
				let tdReimbID = tr.appendChild(document.createElement("td"));
				tdReimbID.setAttribute("id", `reimb-id${i}`);
				
				let tdReimbType = tr.appendChild(document.createElement("td"));
				tdReimbType.setAttribute("id", `type${i}`);
				
				let tdReimbDescription = tr.appendChild(document.createElement("td"));
				tdReimbDescription.setAttribute("id", `description${i}`);
				
				let tdReimbAmount = tr.appendChild(document.createElement("td"));
				tdReimbAmount.setAttribute("id", `amount${i}`);
				
				let tdReimbSubmitted = tr.appendChild(document.createElement("td"));
				tdReimbSubmitted.setAttribute("id", `submitted${i}`);
				
				let tdReimbResolved = tr.appendChild(document.createElement("td"));
				tdReimbResolved.setAttribute("id", `resolved${i}`);
				
				let tdReimbResolver = tr.appendChild(document.createElement("td"));
				tdReimbResolver.setAttribute("id", `resolver${i}`);
				
				let tdStatus = tr.appendChild(document.createElement("td"));
				tdStatus.setAttribute("id", `status${i}`);

				document.getElementById(`reimb-id${i}`).innerHTML = reimb.reimb_id;
				document.getElementById(`type${i}`).innerHTML = reimb.reimb_type_id;
				document.getElementById(`description${i}`).innerHTML = reimb.reimb_description;
				document.getElementById(`amount${i}`).innerHTML = "$" + parseFloat(reimb.reimb_amount).toFixed(2);

				if(reimb.submitted != null){
				var date = new Date(reimb.reimb_submitted);
				var formattedDate = (date.getMonth() + 1) + "-" + date.getDate() + "-" + date.getFullYear();
				var hours = (date.getHours() < 10) ? "0" + date.getHours() : date.getHours();
				var minutes = (date.getMinutes() < 10) ? "0" + date.getMinutes() : date.getMinutes();
				var formattedTime = hours + ":" + minutes;
				document.getElementById(`submitted${i}`).innerHTML = formattedDate + " " + formattedTime;
				} else {
					document.getElementById(`submitted{i}`).innerHTML = "N/A";
				}

				if (reimb.resolved != null) {
					var date = new Date(reimb.reimb_resolved);
					var formattedDate = (date.getMonth() + 1) + "-" + date.getDate() + "-" + date.getFullYear();
					var hours = (date.getHours() < 10) ? "0" + date.getHours() : date.getHours();
					var minutes = (date.getMinutes() < 10) ? "0" + date.getMinutes() : date.getMinutes();
					var formattedTime = hours + ":" + minutes;
					document.getElementById(`resolved${i}`).innerHTML = formattedDate + " " + formattedTime;
				} else {
					document.getElementById(`resolved${i}`).innerHTML = "N/A";
				}
				document.getElementById(`resolver${i}`).innerHTML = reimb.reimb_resolver;
				document.getElementById(`status${i}`).innerHTML = reimb.reimb_status_id;
				i++;
			}
			getDetails();
		}
	}
	xhttp.open('GET', 'http://localhost:8080/Proj1/getTickets.json');
	xhttp.send();
}

function getDetails() {
	let xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			let details = JSON.parse(xhttp.responseText);
			console.log(details);
			let j = 1;
			for (let i = 0; i < details[0].length; i++) {
				document.getElementById(`type${j}`).innerHTML = details[0][i];
				if (details[1][i]) {
					document.getElementById(`resolver${j}`).innerHTML = details[1][i];
				} else {
					document.getElementById(`resolver${j}`).innerHTML = "N/A";
				}
				document.getElementById(`status${j}`).innerHTML = details[2][i];
				j++;
			}
		}
	}

	xhttp.open('GET', 'http://localhost:8080/Proj1/getDetails.json');

	xhttp.send();
}
