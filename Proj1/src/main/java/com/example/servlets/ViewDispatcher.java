package com.example.servlets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.example.controller.Controller;
import com.example.dao.Proj1Con;

public class ViewDispatcher {
	private static final Logger log = Logger.getLogger(ViewDispatcher.class);
	
	public String process(HttpServletRequest req, HttpServletResponse res) {
		switch (req.getRequestURI()) {
		case "/Proj1/login.change":
			System.out.println("in view dispatcher, login.change");
			return Controller.login(req);

		case "/Proj1/logout.change":
			System.out.println("in view dispatcher, logout.change");
			return Controller.logout(req);

		case "/Proj1/registerUser.change":
			System.out.println("in view dispatcher, register.change");
			return Controller.registerUser(req);

		case "/Proj1/viewHistory.change":
			System.out.println("in view dispatcher, viewHistory.change");
			return Controller.viewHistory(req);

		case "/Proj1/reimbursement.change":
			System.out.println("in view dispatcher, reimbursement.change");
			return Controller.addReimbursement(req);

		case "/Proj1/viewTickets.change":
			System.out.println("in view dispatcher, viewTickets.change");
			return Controller.viewTickets(req);

		case "/Proj1/EmployeeMain.change":
			System.out.println("in view dispatcher, EmployeeMain.change");
			return Controller.redirectEmployeeHome(req);

		case "/Proj1/ManagerMain.change":
			System.out.println("in view dispatcher, ManagerMain.change");
			return Controller.redirectManagerHome(req);

		case "/Proj1/approve.change":
			System.out.println("in view dispatcher, approve.change");
			return Controller.approve(req);

		case "/Proj1/deny.change":
			System.out.println("in view dispatcher, deny.change");
			return Controller.deny(req);
		
		case "/Proj1/filterPending.change":
			System.out.println("in view dispatcher, filterPending.change");
			return Controller.filterPending(req);

		case "/Proj1/filterApproved.change":
			System.out.println("in view dispatcher, filterApproved.change");
			return Controller.filterApproved(req);

		case "/Proj1/filterDenied.change":
			System.out.println("in view dispatcher, filterPending.change");
			return Controller.filterDenied(req);
		 
		default:
			System.out.println("in view dispatcher, default");
			return "html/FailedLogin.html";
		}
	}
}
