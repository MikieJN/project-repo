package com.example;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import com.example.dao.Proj1Con;
import com.example.dao.ReimbDaoImp;
import com.example.dao.UserDaoImp;
import com.example.model.Reimbursement;
import com.example.model.User;
import com.example.service.Service;

public class MainDriver {
	
	public static void main(String[] args) {
		Proj1Con pCon = new Proj1Con();
		UserDaoImp uDao = new UserDaoImp(pCon);
		ReimbDaoImp rDao = new ReimbDaoImp(pCon);
		Service serv = new Service(uDao, rDao);
		
		List<Reimbursement> pending = serv.getFilteredTickets(1);
		for (Reimbursement reimb : pending) {
			System.out.println(reimb);
		}
		List<Reimbursement> approved = serv.getFilteredTickets(2);
		for (Reimbursement reimb : approved) {
			System.out.println(reimb);
		}
		List<Reimbursement> denied = serv.getFilteredTickets(0);
		for (Reimbursement reimb : denied) {
			System.out.println(reimb);
		}

	}
}
