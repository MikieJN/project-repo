package com.example.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Proj1Con {
	
	ClassLoader classLoader = getClass().getClassLoader();
	InputStream is;
	Properties p = new Properties();
	
	public Proj1Con() {
		is = classLoader.getResourceAsStream("connection.properties");
		try {
			p.load(is);
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public Connection getDBConnection() throws SQLException{
		final String URL = "jdbc:postgresql://revdb-2109.cixlhgine7gs.us-east-2.rds.amazonaws.com:5432/project1";
		final String username = "projadmin";
		final String password = "adm1npw";
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return DriverManager.getConnection(URL, username, password);
	}

}
