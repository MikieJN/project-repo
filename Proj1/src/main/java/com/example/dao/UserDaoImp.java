package com.example.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.example.model.User;

public class UserDaoImp {
	
	private Proj1Con pCon;
	
	public UserDaoImp() {
		
	}
	
	public UserDaoImp(Proj1Con pCon) {
		this.pCon = pCon;
	}
	
	public void insertUser(String username, String password, String fname, String lname, String email, int roleID) {
		try(Connection con = pCon.getDBConnection()){
			String sql = "insert into users(user_username, user_password, user_first_name, user_last_name, user_email, user_role_id) values('" + username + "','" + password + "','" + fname + "'," + lname +
					"','" + email + "'," + roleID + ")";
			
			Statement statement = con.createStatement();
			int changed = statement.executeUpdate(sql);
			System.out.println("Number of rows changed: " + changed);
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void preparedInsertUser(String username, String password, String fname, String lname, String email, int roleID) {
		try(Connection con = pCon.getDBConnection()){
			String sql = "insert into users(user_username, user_password, user_first_name, user_last_name, user_email, user_role_id) values(?,?,?,?,?,?)";
			PreparedStatement prepared = con.prepareStatement(sql);
			prepared.setString(1, username);
			prepared.setString(2, password);
			prepared.setString(3, fname);
			prepared.setString(4, lname);
			prepared.setString(5, email);
			prepared.setInt(6, roleID);
			
			int changed = prepared.executeUpdate(sql);
			System.out.println("Number of rows changed: " + changed);
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void callableInsertUser(String username, String password, String fname, String lname, String email, int roleID) {
		try(Connection con = pCon.getDBConnection()){
			String sql = "{? = call insert_user(?,?,?,?,?,?)}";
			
			CallableStatement cs = con.prepareCall(sql);
			
			cs.registerOutParameter(1, Types.VARCHAR);
			cs.setString(2, username);
			cs.setString(3, password);
			cs.setString(4, fname);
			cs.setString(5, lname);
			cs.setString(6, email);
			cs.setInt(7, roleID);
			cs.execute();
			
			System.out.println(cs.getString(1));
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
	
	public List<User> getAllUsers(){
		List<User> userList = new ArrayList<>();
		
		try(Connection con = pCon.getDBConnection()){
			String sql = "select * from users";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
						int userID = rs.getInt(1); 
						String username = rs.getString(2); 
						String password = rs.getString(3); 
						String fname = rs.getString(4);
						String lname = rs.getString(5); 
						String email = rs.getString(6); 
						int roleID = rs.getInt(7);
						
						userList.add(new User(userID, username, password, fname, lname, email, roleID));
			}
			
			return userList;
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void insertUser(User user) {
		try(Connection con = pCon.getDBConnection()){
			String sql = "{? = call insert_user(?,?,?,?,?,?)}";
			
			CallableStatement cs = con.prepareCall(sql);
			
			cs.registerOutParameter(1, Types.VARCHAR);
			cs.setString(2, user.getUsername());
			cs.setString(3, user.getPassword());
			cs.setString(4, user.getFname());
			cs.setString(5, user.getLname());
			cs.setString(6, user.getEmail());
			cs.setInt(7, user.getRoleID());
			cs.execute();
			
			System.out.println(cs.getString(1));
			} catch(SQLException e) {
				e.printStackTrace();
			}
		
	}
	
	public String getUserRole(int ID) {
		try(Connection con = pCon.getDBConnection()){
			String sql = "{? = call get_user_role(?)}";
			CallableStatement cs = con.prepareCall(sql);
			cs.registerOutParameter(1, Types.VARCHAR);
			cs.setInt(2, ID);
			cs.execute();
			
			return cs.getString(1);
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public User getUserByUsername(String username) {
		UserDaoImp uDao = new UserDaoImp(pCon);
		for(User user: uDao.getAllUsers()) {
			if(user.getUsername().equals(username)) {
				return user;
				
			}
		}
			
		return null;
	}

	public String getFullNameByID(int ID) {
		String name = "";
		try(Connection con = pCon.getDBConnection()){
			String sql = "select user_first_name, user_last_name from users where user_id = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, ID);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				name = rs.getString(1) + " " + rs.getString(2);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
