package com.example.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.example.model.Reimbursement;

public class ReimbDaoImp {
	
	private Proj1Con pCon = new Proj1Con();
	private static final Logger log = Logger.getLogger(ReimbDaoImp.class);
	
	public ReimbDaoImp() {
		
	}
	
	public ReimbDaoImp(Proj1Con pCon) {
		this.pCon = pCon;
	}
	
	public void insertReimb(double amount, Date submitted, Date resolved, String description, String receipt, int authorID, int resolverID, int statusID, int typeID) {
		try(Connection con = pCon.getDBConnection()){
			String sql = "insert into reimbursement(reimb_amount, reimb_submitted, reimb_resolved, reimb_description, reimb_receipt, reimb_author_id, reimb_resolver_id, reimb_status_id, reimb_type_id)"
					+ "values('" + amount + "','" + submitted + "','" + resolved + "','" + description + "','" + receipt + "','" + authorID + "','" + resolverID + "','" + statusID + "','" + typeID +"')";
			
			Statement statement = con.createStatement();
			int changed = statement.executeUpdate(sql);
			System.out.println("Number of rows changed: " + changed);
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void preparedInsertReimb(double amount, Date submitted, Date resolved, String description, String receipt, int authorID, int resolverID, int statusID, int typeID) {
		try(Connection con = pCon.getDBConnection()){
			String sql = "insert into reimbursement(reimb_amount, reimb_submitted, reimb_resolved, reimb_description, reimb_receipt, reimb_author_id, reimb_resolver_id, reimb_status_id, reimb_type_id)"
					+ "values(?,?,?,?,?,?,?,?,?)";
			
			PreparedStatement prepared = con.prepareStatement(sql);
			prepared.setDouble(1, amount);
			prepared.setDate(2, (java.sql.Date) submitted);
			prepared.setDate(3, null);
			prepared.setString(4, description);
			prepared.setString(5, receipt);
			prepared.setInt(6, authorID);
			prepared.setInt(7, resolverID);
			prepared.setInt(8, statusID);
			prepared.setInt(9, typeID);
			
			int changed = prepared.executeUpdate(sql);
			System.out.println("Number of rows changed: " + changed);
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void callableInsertReimb(double amount, Date submitted, Date resolved, String description, String receipt, int authorID, int resolverID, int statusID, int typeID) {
		try(Connection con = pCon.getDBConnection()){
			String sql = "{? = call insert_reimb(?, ?, ?, ?, ?, ?, ?, ?, ?)}";
			
			CallableStatement cs = con.prepareCall(sql);
			
			cs.registerOutParameter(1, Types.VARCHAR);
			cs.setDouble(2, amount);
			cs.setDate(3, submitted);
			cs.setDate(4, resolved);
			cs.setString(5, description);
			cs.setString(6, receipt);
			cs.setInt(7, authorID);
			cs.setInt(8, resolverID);
			cs.setInt(9, statusID);
			cs.setInt(10, typeID);
			cs.execute();
			
			System.out.println(cs.getString(1));
			
		} catch(SQLException e) {
			e.printStackTrace();
			log.fatal("SQL Exception");
		}
	}

	public List<Reimbursement> getAllReimbs() {
		List<Reimbursement> reimbList = new ArrayList<>();
		
		try(Connection con = pCon.getDBConnection()){
			String sql = "select * from reimbursement";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				int reimbID = rs.getInt(1);
				double amount = rs.getDouble(2);
				Date submitted = rs.getDate(3);
				Date resolved = rs.getDate(4);
				String description = rs.getString(5);
				String receipt = rs.getString(6);
				int authorID = rs.getInt(7);
				int resolverID = rs.getInt(8);
				int statusID = rs.getInt(9);
				int typeID = rs.getInt(10);
				
				reimbList.add(new Reimbursement(reimbID, amount, submitted, resolved, description, receipt, authorID, resolverID, statusID, typeID));
			}
			
			return reimbList;
			
		} catch(SQLException e) {
			e.printStackTrace();
			log.fatal("SQL Exception");
		}
		
		return null;
	}
	
	public String getReimbType(int reimbID) {
		try(Connection con = pCon.getDBConnection()){
			String sql = "{? = call get_reimb_type(?)}";
			
			CallableStatement cs = con.prepareCall(sql);
			cs.registerOutParameter(1, Types.VARCHAR);
			cs.setInt(2, reimbID);
			cs.execute();
			
			return cs.getString(1);
		} catch (SQLException e) {
			log.fatal("SQL Exception");
		}
		
		return null;
	}

	public int getNewReimbID() {
		int id = 0;
		
		try(Connection con = pCon.getDBConnection()){
			String sql = "select max(reimb_id) from reimbursement";
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			
			while(rs.next()) {
				id = rs.getInt(1);
			}
			return id;
		} catch(SQLException e) {
			log.fatal("SQL Exception");
		}
		return id;
	}

	public void insertReimb(Reimbursement reimb) {
		try(Connection con = pCon.getDBConnection()){
			String sql = "{? = call insert_reimb(?,?,?,?,?,?,?,?,?)}";
			
			CallableStatement cs = con.prepareCall(sql);
			
			cs.registerOutParameter(1, Types.VARCHAR);
			cs.setDouble(2, reimb.getAmount());
			cs.setDate(3, null);
			cs.setDate(4, null);
			cs.setString(5, reimb.getDescription());
			cs.setString(6, reimb.getReceipt());
			cs.setInt(7, reimb.getAuthorID());
			cs.setInt(8, reimb.getResolverID());
			cs.setInt(9, reimb.getStatusID());
			cs.setInt(10, reimb.getTypeID());
			cs.execute();
			
			System.out.println(cs.getString(1));
			
		} catch(SQLException e) {
			e.printStackTrace();
			log.fatal("SQL Exception");
		}
	}

	public void updateReimb(Reimbursement reimb) {
		// TODO Auto-generated method stub
		
	}

	public void insertJunction(int userID, int reimbid) {
		try(Connection con = pCon.getDBConnection()){
			String sql = "{? = call add_relationship(?,?)}";
			CallableStatement cs = con.prepareCall(sql);
			cs.registerOutParameter(1, Types.VARCHAR);
			cs.setInt(2, userID);
			cs.setInt(3, reimbid);
			cs.execute();
			log.info(cs.getString(1));
		} catch(SQLException e) {
			log.fatal("SQL Exception");
		}
		
	}

	public Reimbursement getReimbByID(int reimbID) {
		Reimbursement reimb = null;
		try(Connection con = pCon.getDBConnection()){
			String sql = "select * from reimbursement where reimb_id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, reimbID);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				reimb = new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getDate(3), rs.getDate(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10));
			}
			return reimb;
		} catch(SQLException e) {
			log.fatal("SQL Exception");
		}
		return null;
	}

	public List<Reimbursement> filterReimbs(int reimbID) {
		List<Reimbursement> reimbList = new ArrayList<>();
		try(Connection con = pCon.getDBConnection()){
			String sql = "select * from reimbursement where status_id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, reimbID);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				reimbList.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getDate(3), rs.getDate(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10)));
			}
			
			return reimbList;
		}catch(SQLException e) {
			log.fatal("SQL Exception");
		}
		return null;
	}

	public List<Integer> getUserReimbIDs(int userID) {
		List<Integer> idList = new ArrayList<>();
		
		try(Connection con = pCon.getDBConnection()){
			String sql = "select r_id from user_reimbursement_junc where u_id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, userID);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				idList.add(new Integer(rs.getInt(1)));
			}
			
			return idList;
		} catch(SQLException e) {
			e.printStackTrace();
			log.fatal("SQL Exception");
		}
		return null;
	}

	public String getReimbStatus(int statusID) {
		try(Connection con = pCon.getDBConnection()){
			String sql = "{? = call get_reimb_status(?)}";
			CallableStatement cs = con.prepareCall(sql);
			cs.registerOutParameter(1, Types.VARCHAR);
			cs.setInt(2, statusID);
			cs.execute();
			
			return cs.getString(1);
		} catch(SQLException e) {
			log.fatal("SQL Exception");
		}
		return null;
	}

}
