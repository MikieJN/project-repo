package com.example.model;

public class ReimbursementStatus {
	
	private int statusID;
	private String status;
	
	public ReimbursementStatus() {
		
	}
	
	public ReimbursementStatus(String status) {
		this.status = status;
	}
	
	public ReimbursementStatus(int statusID, String status) {
		this.statusID = statusID;
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getStatusID() {
		return statusID;
	}

	@Override
	public String toString() {
		return "ReimbursementStatus [statusID=" + statusID + ", status=" + status + "]";
	}
	
	

}
