package com.example.model;

public class ReimbursementType {
	
	private int typeID;
	private String type;
	
	public ReimbursementType() {
		
	}
	
	public ReimbursementType(String type) {
		this.type = type;
	}
	
	public ReimbursementType(int typeID, String type) {
		this.typeID = typeID;
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getTypeID() {
		return typeID;
	}

	@Override
	public String toString() {
		return "ReimbursementType [typeID=" + typeID + ", type=" + type + "]";
	}
	
	

}
