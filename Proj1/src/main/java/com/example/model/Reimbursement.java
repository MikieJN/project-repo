package com.example.model;

import java.sql.Date;

public class Reimbursement {
	
	private int reimbID;
	private double amount;
	private Date submitted;
	private Date resolved;
	private String description;
	private String receipt;
	private int authorID;
	private int resolverID;
	private int statusID;
	private int typeID;
	
	public Reimbursement() {
		
	}
	
	public Reimbursement(int reimbID, double amount, Date submitted, Date resolved, String description, String receipt, int authorID, int resolverID, int statusID, int typeID) {
		
		this.reimbID = reimbID;
		this.amount = amount;
		this.submitted = submitted;
		this.resolved = resolved;
		this.description = description;
		this.receipt = receipt;
		this.authorID = authorID;
		this.resolverID = resolverID;
		this.statusID = statusID;
		this.typeID = typeID;
		
	}
	
public Reimbursement(int reimbID, double amount, String description, String receipt, int authorID, int resolverID, int statusID, int typeID) {
		
		this.reimbID = reimbID;
		this.amount = amount;
		this.description = description;
		this.receipt = receipt;
		this.authorID = authorID;
		this.resolverID = resolverID;
		this.statusID = statusID;
		this.typeID = typeID;
		
	}

public Reimbursement(double amount, Date submitted, Date resolved, String description, String receipt, int authorID, int resolverID, int statusID, int typeID) {
	
	this.amount = amount;
	this.submitted = submitted;
	this.resolved = resolved;
	this.description = description;
	this.receipt = receipt;
	this.authorID = authorID;
	this.resolverID = resolverID;
	this.statusID = statusID;
	this.typeID = typeID;
	
}

public Reimbursement(double amount, String description, String receipt, int authorID, int resolverID, int statusID, int typeID) {
	
	this.amount = amount;
	this.description = description;
	this.receipt = receipt;
	this.authorID = authorID;
	this.resolverID = resolverID;
	this.statusID = statusID;
	this.typeID = typeID;
	
}

public double getAmount() {
	return amount;
}

public void setAmount(double amount) {
	this.amount = amount;
}

public Date getSubmitted() {
	return submitted;
}

public void setSubmitted(Date submitted) {
	this.submitted = submitted;
}

public Date getResolved() {
	return resolved;
}

public void setResolved(Date resolved) {
	this.resolved = resolved;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public String getReceipt() {
	return receipt;
}

public void setReceipt(String receipt) {
	this.receipt = receipt;
}

public int getAuthorID() {
	return authorID;
}

public void setAuthorID(int authorID) {
	this.authorID = authorID;
}

public int getResolverID() {
	return resolverID;
}

public void setResolverID(int resolverID) {
	this.resolverID = resolverID;
}

public int getStatusID() {
	return statusID;
}

public void setStatusID(int statusID) {
	this.statusID = statusID;
}

public int getTypeID() {
	return typeID;
}

public void setTypeID(int typeID) {
	this.typeID = typeID;
}

public int getReimbID() {
	return reimbID;
}

@Override
public String toString() {
	return "Reimbursement [reimbID=" + reimbID + ", amount=" + amount + ", submitted=" + submitted + ", resolved="
			+ resolved + ", description=" + description + ", receipt=" + receipt + ", authorID=" + authorID
			+ ", resolverID=" + resolverID + ", statusID=" + statusID + ", typeID=" + typeID + "]";
}

}
