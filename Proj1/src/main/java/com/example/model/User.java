package com.example.model;

public class User {
	
	private int userID;
	private String username;
	private String password;
	private String fname;
	private String lname;
	private String email;
	private int roleID;
	
public User() {
		
	}
	
	public User(String username, String password, String fname, String lname, String email, int roleID) {
		super();
		this.username = username;
		this.password = password;
		this.fname = fname;
		this.lname = lname;
		this.email = email;
		this.roleID = roleID;
	}
	
	public User(int userID, String username, String password, String fname, String lname, String email, int roleID) {
		super();
		this.userID = userID;
		this.username = username;
		this.password = password;
		this.fname = fname;
		this.lname = lname;
		this.email = email;
		this.roleID = roleID;
	}
	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getRoleID() {
		return roleID;
	}

	public void setRoleID(int roleID) {
		this.roleID = roleID;
	}

	public int getUserID() {
		return userID;
	}
	
	public void setUserID() {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "User [userID=" + userID + ", username=" + username + ", password=" + password + ", fname=" + fname
				+ ", lname=" + lname + ", email=" + email + ", roleID=" + roleID + "]";
	}

}
