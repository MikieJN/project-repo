package com.example.model;

public class UserRole {
	
	private int roleID;
	private String role;
	
	public UserRole() {
		
	}
	
	public UserRole(String role) {
		this.role = role;
	}
	
	public UserRole(int roleID, String role) {
		this.roleID = roleID;
		this.role = role;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public int getRoleID() {
		return roleID;
	}

	@Override
	public String toString() {
		return "UserRole [roleID=" + roleID + ", role=" + role + "]";
	}
	
	

}
