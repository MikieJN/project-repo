package com.example.service;

import java.util.ArrayList;
import java.util.List;

import com.example.dao.ReimbDaoImp;
import com.example.dao.UserDaoImp;
import com.example.model.Reimbursement;
import com.example.model.User;

public class Service {
	private final UserDaoImp uDao;
	private final ReimbDaoImp rDao;
	
	public Service() {
		this.uDao = new UserDaoImp();
		this.rDao = new ReimbDaoImp();
	}

	public Service(UserDaoImp uDao, ReimbDaoImp rDao) {
		super();
		this.uDao = uDao;
		this.rDao = rDao;
	}
	
	public List<User> getAllUsers() {
		return uDao.getAllUsers();
	}
	
	public List<Reimbursement> getAllReimbursements() {
		return rDao.getAllReimbs();
	}
	
	public int getRecentReimb() {
		return rDao.getNewReimbID();
	}
	
	public void insertUser(User user) {
		uDao.insertUser(user);
	}
	
	public void insertReimb(Reimbursement reimb) {
		rDao.insertReimb(reimb);
	}
	
	public void updateReimb(Reimbursement reimb) {
		rDao.updateReimb(reimb);
	}
	
	public void addRelationship(int userID, int reimbid) {
		rDao.insertJunction(userID, reimbid);
	}
	
	public User login(String username, String password) {
		User user = uDao.getUserByUsername(username);
		if (user != null) {
			if (user.getPassword().equals(password)) {
				return user;
			}
		}
		return null;
	}
	
	public Reimbursement getReimbInfo(int reimb_id) {
		return rDao.getReimbByID(reimb_id);
	}
	
	public List<Reimbursement> getFilteredTickets(int reimb_id) {
		return rDao.filterReimbs(reimb_id);
	}
	
	public List<Reimbursement> getPastTickets(int userID) {
		List<Integer> reimbIDList = rDao.getUserReimbIDs(userID);
		List<Reimbursement> reimbList = new ArrayList<>();
		for (int reimbID : reimbIDList) {
			reimbList.add(rDao.getReimbByID(reimbID));
		}
		return reimbList;
	}
	
	public String getAuthor(int authID) {
		return uDao.getFullNameByID(authID);
	}
	
	public String getResolver(int authID) {
		return uDao.getFullNameByID(authID);
	}
	
	public String getType(int typeID) {
		return rDao.getReimbType(typeID);
	}
	
	public String getStatus(int statusID) {
		return rDao.getReimbStatus(statusID);
	}
}
