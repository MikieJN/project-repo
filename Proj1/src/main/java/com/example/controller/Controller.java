package com.example.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.log4j.Logger;

import com.example.dao.Proj1Con;
import com.example.dao.ReimbDaoImp;
import com.example.dao.UserDaoImp;
import com.example.model.Reimbursement;
import com.example.model.User;
import com.example.service.Service;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Controller {
	private static final Logger log = Logger.getLogger(HttpServletRequest.class);
	private static Proj1Con pCon = new Proj1Con();
	private static UserDaoImp uDao = new UserDaoImp(pCon);
	private static ReimbDaoImp rDao = new ReimbDaoImp(pCon);
	private static Service serv = new Service(uDao, rDao);
	
	// JSONDispatcher Functions
	public static void getSessionUser(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		System.out.println("in controller getSessionUser");
		User user = (User)req.getSession().getAttribute("currentUser");
		res.getWriter().write(new ObjectMapper().writeValueAsString(user)); 
	}
	
	public static void getTickets(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		System.out.println("in controller getSessionTickets");
		List<Reimbursement> tickets = (List<Reimbursement>) req.getSession().getAttribute("tickets");
		res.getWriter().write(new ObjectMapper().writeValueAsString(tickets));
	}
	
	public static void getDetails(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		System.out.println("in controller, getDetails");
		List<List<String>> details = (List<List<String>>) req.getSession().getAttribute("details"); 
		res.getWriter().write(new ObjectMapper().writeValueAsString(details));
	}
	
	public static void getAllDetails(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		System.out.println("in controller, getAllDetails");
		List<List<String>> details = (List<List<String>>) req.getSession().getAttribute("details"); 
		res.getWriter().write(new ObjectMapper().writeValueAsString(details));
	}
	
	
	public static String login(HttpServletRequest req) { 
		System.out.println("in controller login");
		log.trace("Controller login() is running...");
		
		if (!req.getMethod().equals("POST")) {
			System.out.println("get method not post");
			return "html/FailedLogin.html";
		} 
		
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		
		User user = serv.login(username, password);
		if (user == null) {
			System.out.println("User not found " + user);
			return "wrongcredentials.change";
		} else {
			req.getSession().setAttribute("currentUser", user);
			if (user.getRoleID() == 1) {
				
				return "html/ManagerMain.html";
			} else {
				
				return "html/EmployeeMain.html";
			}
		}
	}
	
	public static String logout(HttpServletRequest req) {
		HttpSession sesh = req.getSession();
		sesh.invalidate();
		return "html/redirectPage.html";
	}
	
	public static String registerUser (HttpServletRequest req) { 
		System.out.println("in controller register");
		log.trace("Controller register() is running...");
		
		if (!req.getMethod().equals("POST")) { 
			return "html/FailedLogin.html";
		} 
		// process out the information that is sent in the request
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		String fname = req.getParameter("fname");
		String lname = req.getParameter("lname");
		String email = req.getParameter("email");

		serv.insertUser(new User(username, password, fname, lname, email, 0));
		return "html/redirectPage.html";
	}
	
	public static String addReimbursement(HttpServletRequest req) { 
		System.out.println("in controller addRequest");
		log.trace("Controller addReimbursement() is running...");
		User user = (User) req.getSession().getAttribute("currentUser");
		
		if (!req.getMethod().equals("POST")) { 
			return "html/FailedLogin.html";
		} 
		
		// process out the information that is sent in the request
		String amountPart = req.getParameter("amount");
		double amount = Double.parseDouble(amountPart);
		
		String description = req.getParameter("description");
		
		String receipt = "Auto generated receipt";
		
		int type = -1;
		switch(req.getParameter("reimbursement-type")) {
			case "travel":
				type = 0;
				break;
			case "lodging":
				type = 1;
				break;
			case "food":
				type = 2;
				break;
			case "other":
				type = 3;
		}
		
		// getting the submitted time
		long millis = System.currentTimeMillis();
		java.sql.Date date = new java.sql.Date(millis);
		java.sql.Timestamp currTime = new java.sql.Timestamp(date.getTime());
		
		// add reimbursement request to database
		System.out.println("amount:" + amount + " date:" + date + " description:" + description + " receipt:" + receipt + " userid" + user.getUserID() + " type:" + type);
		serv.insertReimb(new Reimbursement(amount, date, date, description, receipt, user.getUserID(), 0, 0, type));
		
		// update junction table
		serv.addRelationship(user.getUserID(), serv.getRecentReimb());
		
		return "html/EmployeeMain.html";
	}
	
	public static String viewHistory(HttpServletRequest req) {
		System.out.println("in controller viewHistory");
		log.trace("Controller viewHistory() is running...");
		User user = (User) req.getSession().getAttribute("currentUser");
		
		List<Reimbursement> reimbList = serv.getPastTickets(user.getUserID());
		System.out.println(reimbList);
		List<String> typeDetails = new ArrayList<>();
		
		List<String> resolverDetails = new ArrayList<>();
		
		List<String> statusDetails = new ArrayList<>();
		
		for (Reimbursement reimb : reimbList) {
			typeDetails.add(serv.getType(reimb.getTypeID()));
			resolverDetails.add(serv.getResolver(reimb.getResolverID()));
			statusDetails.add(serv.getStatus(reimb.getStatusID()));
		}
		
		System.out.println(typeDetails);
		System.out.println(resolverDetails);
		System.out.println(statusDetails);
		
		List<List<String>> details = Arrays.asList(typeDetails, resolverDetails, statusDetails);
		req.getSession().setAttribute("tickets", reimbList);
		req.getSession().setAttribute("details", details);
		saveUser(req);
		return "html/view-session-tickets.html";
	}
	
	public static String filterPending(HttpServletRequest req) {
		System.out.println("in controller filterPending");
		log.trace("Controller filterPending() is running...");
		List<Reimbursement> pendingTickets = serv.getFilteredTickets(1);
		List<String> typeDetails = new ArrayList<>();
		List<String> authorDetails = new ArrayList<>();
		List<String> resolverDetails = new ArrayList<>();
		List<String> statusDetails = new ArrayList<>();
		for (Reimbursement reimb : pendingTickets) {
			typeDetails.add(serv.getType(reimb.getTypeID()));
			authorDetails.add(serv.getAuthor(reimb.getAuthorID()));
			resolverDetails.add(serv.getResolver(reimb.getResolverID()));
			statusDetails.add(serv.getStatus(reimb.getStatusID()));
		}
		List<List<String>> details = Arrays.asList(typeDetails, authorDetails, resolverDetails, statusDetails);
		req.getSession().setAttribute("details", details);
		req.getSession().setAttribute("tickets", pendingTickets);
		saveUser(req);
		return "html/pendingReimbursements.html";
	}
	
	public static String filterApproved(HttpServletRequest req) {
		System.out.println("in controller filterApproved");
		log.trace("Controller filterApproved() is running...");
		List<Reimbursement> approvedTickets = serv.getFilteredTickets(2);
		List<String> typeDetails = new ArrayList<>();
		List<String> authorDetails = new ArrayList<>();
		List<String> resolverDetails = new ArrayList<>();
		List<String> statusDetails = new ArrayList<>();
		for (Reimbursement reimb : approvedTickets) {
			typeDetails.add(serv.getType(reimb.getTypeID()));
			authorDetails.add(serv.getAuthor(reimb.getAuthorID()));
			resolverDetails.add(serv.getResolver(reimb.getResolverID()));
			statusDetails.add(serv.getStatus(reimb.getStatusID()));
		}
		List<List<String>> details = Arrays.asList(typeDetails, authorDetails, resolverDetails, statusDetails);
		req.getSession().setAttribute("details", details);
		req.getSession().setAttribute("tickets", approvedTickets);
		saveUser(req);
		return "html/approvedReimbs.html";
	}
	
	public static String filterDenied(HttpServletRequest req) {
		System.out.println("in controller filterDenied");
		log.trace("Controller filterDenied() is running...");
		List<Reimbursement> deniedTickets = serv.getFilteredTickets(0);
		List<String> typeDetails = new ArrayList<>();
		List<String> authorDetails = new ArrayList<>();
		List<String> resolverDetails = new ArrayList<>();
		List<String> statusDetails = new ArrayList<>();
		for (Reimbursement reimb : deniedTickets) {
			typeDetails.add(serv.getType(reimb.getTypeID()));
			authorDetails.add(serv.getAuthor(reimb.getAuthorID()));
			resolverDetails.add(serv.getResolver(reimb.getReimbID()));
			statusDetails.add(serv.getStatus(reimb.getStatusID()));
		}
		List<List<String>> details = Arrays.asList(typeDetails, authorDetails, resolverDetails, statusDetails);
		req.getSession().setAttribute("details", details);
		req.getSession().setAttribute("tickets", deniedTickets);
		saveUser(req);
		return "html/deniedReimbursements.html";
	}
	
	public static String viewTickets(HttpServletRequest req) {
		System.out.println("in controller viewTickets");
		log.trace("Controller viewTickets() is running...");
		List<Reimbursement> rList = serv.getAllReimbursements();
		List<String> typeDetails = new ArrayList<>();
		List<String> authorDetails = new ArrayList<>();
		List<String> resolverDetails = new ArrayList<>();
		List<String> statusDetails = new ArrayList<>();
		for (Reimbursement reimb : rList) {
			typeDetails.add(serv.getType(reimb.getTypeID()));
			authorDetails.add(serv.getAuthor(reimb.getAuthorID()));
			resolverDetails.add(serv.getResolver(reimb.getResolverID()));
			statusDetails.add(serv.getStatus(reimb.getStatusID()));
		}
		List<List<String>> details = Arrays.asList(typeDetails, authorDetails, resolverDetails, statusDetails);
		req.getSession().setAttribute("tickets", rList);
		req.getSession().setAttribute("details", details);
		saveUser(req);
		return "html/viewTickets.html";
	}

	public static String redirectEmployeeHome(HttpServletRequest req) {
		saveUser(req);
		return "html/EmployeeMain.html";
	}
	
	public static String redirectManagerHome(HttpServletRequest req) {
		saveUser(req);
		return "html/ManagerMain.html";
	}

	public static String approve(HttpServletRequest req) {
		System.out.println("in controller approve");
		log.trace("Controller approve() is running...");
		User user = (User) req.getSession().getAttribute("currentUser");
		
		if (!req.getMethod().equals("POST")) {
			return "html/FailedLogin.html";
		} 
		String id = req.getParameter("id1");
		Reimbursement reimb = serv.getReimbInfo(Integer.parseInt(id));
		reimb.setStatusID(2);
		reimb.setResolverID(user.getUserID());
		long millis = System.currentTimeMillis();
		java.sql.Date date = new java.sql.Date(millis);
		java.sql.Timestamp currTime = new java.sql.Timestamp(date.getTime());
		reimb.setResolved(date);
		serv.updateReimb(reimb);
		saveUser(req);
		return viewTickets(req);
	}
	
	public static String deny(HttpServletRequest req) {
		System.out.println("in controller deny");
		log.trace("Controller deny() is running...");
		User user = (User) req.getSession().getAttribute("currentUser");
		
		if (!req.getMethod().equals("POST")) { 
			return "html/FailedLogin.html";
		} 
		
		String id = req.getParameter("id2");
		Reimbursement reimb = serv.getReimbInfo(Integer.parseInt(id));
	
		reimb.setStatusID(0);
		reimb.setResolverID(user.getUserID());
		long millis = System.currentTimeMillis();
		java.sql.Date date = new java.sql.Date(millis);
		java.sql.Timestamp currTime = new java.sql.Timestamp(date.getTime());
		reimb.setResolved(date);
		serv.updateReimb(reimb);
		return viewTickets(req);
	}
	
	public static void saveUser(HttpServletRequest req) {
		User user = (User) req.getSession().getAttribute("currentUser");
		req.getSession().setAttribute("currentUser", user);
	}	
}
