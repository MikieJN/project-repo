package com.example.screen;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.example.dao.AccountDaoImp;
import com.example.dao.Proj0DBConnection;
import com.example.dao.UserDaoImp;
import com.example.model.Account;
import com.example.model.User;

public class LoginScreen {
	
	private Proj0DBConnection pCon = new Proj0DBConnection();
	
	public LoginScreen() {
		
	}
	
	public LoginScreen(Proj0DBConnection pCon) {
		this.pCon = pCon;
	}
	
	public void LoginCustomer() {
		String fname;
		String lname;
		String password;
		String userType = "C";
		
		
		int choice;
		double dchoice;
		double wchoice;
		double tchoice;
		
		User u = new User();
		Account a = new Account();
		
		int an1;
		int an2;
		
		String accountType;
		double accountFunds;
		String accountStatus = "P";
		List<Account> acct = new ArrayList<>();
		
		String sql;
		
		UserDaoImp uDao = new UserDaoImp(pCon);
		AccountDaoImp aDao = new AccountDaoImp(pCon);

System.out.println("You are in the customer login page.");
System.out.println("Enter your first name: ");
		
		Scanner sc = new Scanner(System.in);
		
		fname = sc.nextLine();
		
		
		System.out.println("Enter your last name: ");
		
		lname = sc.nextLine();
		
		
		System.out.println("Enter your password: ");
		
		password = sc.nextLine();
		
		u = uDao.getUserByLogin(fname, lname, password);
		
		if(!u.getUserType().equals("C")) {
			System.out.println("These credentials are not listed for this type account.");
		} else {
		
		
		
		//if(u.equals(u2 = uDao.getUserNoID(fname, lname, password, userType)) == true) {
		
		System.out.println("Welcome " + fname + " you are signed in.");
		System.out.println(u.toString());
		choice = 10;
		while(choice == 10) {
		acct = aDao.getAcctByUserID(u.getUserID());
		
		for(int i = 0; i < acct.size(); i++) {
		a = acct.get(i);
		System.out.println("Your current account balance in " + a.getAccountNum() + " is: " + a.getAccountFunds());
		}
		System.out.println("Select an option below to edit your account.");
		System.out.println("[0] Deposit");
		System.out.println("[1] Withdraw");
		System.out.println("[2] Transfer");
		System.out.println("[3] Apply for new account");
		System.out.println("[9] Logout and quit");
		
		choice = sc.nextInt();
		
		if(choice == 0 && acct.size() == 1) {
			System.out.println("You have selected Deposit. Please enter the amount you wish to deposit.");
			dchoice = sc.nextDouble();
			aDao.deposit(dchoice, a.getAccountFunds(), a.getAccountNum());
			choice = 10;
		} else if(choice == 0 && acct.size() > 1) {
			
		}
		
		else if(choice == 1) {
			System.out.println("You have selected Withdraw. Please enter the amount you wish to withdraw.");
			wchoice = sc.nextDouble();
			aDao.withdraw(wchoice, a.getAccountFunds(), a.getAccountNum());
			choice = 10;
		}
		
		else if(choice == 2) {
			System.out.println("You have selected Transfer. Please enter the amount you wish to transfer.");
			tchoice = sc.nextDouble();
			System.out.println("Select the account number you would like to transfer $" + tchoice + " from.");
			an1 = sc.nextInt();
			System.out.println("Select the account number you would like to transfer $" + tchoice + " into.");
			an2 = sc.nextInt();
			aDao.transfer(an1, an2, tchoice);
			choice = 10;
		}
		
		else if(choice == 3) {
			System.out.println("What kind of account would you like to apply for " + fname + "? (Type 'C' for Checking or 'S' for Savings)");
			accountType = sc.next();
			System.out.println("After next input");
		if(accountType.equalsIgnoreCase("c") == true) {
				System.out.println("You're applying for a checking account, how much money would you like to start the account with? (Valid entries are numbers with decimals.)");
				accountFunds = sc.nextDouble();
				if(accountFunds > 0) {
					
					System.out.println("Your checking account with $" + accountFunds + " is pending approval!");
					aDao.callableInsertAccount("Checking", accountStatus, accountFunds);
					choice = 10;
					
				} else {
					while(accountFunds <= 0) {
						System.out.println("Invalid entry, please enter an amount that is larger than 0 using numbers with decimals.");
						accountFunds = sc.nextDouble();
					}
					System.out.println("Your checking account with $" + accountFunds + " is pending approval!");
					aDao.callableInsertAccount("Checking", accountStatus, accountFunds);
					choice = 10;
				}
				
				
				
			} else if(accountType.equalsIgnoreCase("s") == true) {
				System.out.println("You're applying for a savings account, how much money would you like to start the account with? (Valid entries are numbers with decimals.)");
				accountFunds = sc.nextDouble();
				
			if(accountFunds > 0) {
				System.out.println("Your savings account with $" + accountFunds + " is pending approval!");
				aDao.callableInsertAccount("Saving", accountStatus, accountFunds);
				sc.close();
			} else {
				while(accountFunds <= 0) {
					System.out.println("Invalid entry, please enter an amount that is larger than 0 using numbers with decimals.");
					accountFunds = sc.nextDouble();
				}
				System.out.println("Your savings account with $" + accountFunds + " is pending approval!");
				aDao.callableInsertAccount("Saving", accountStatus, accountFunds);
				sc.close();
			}
			
		  }
			
		}
		
		else if(choice == 9) {
			System.out.println("You have been logged out! Goodbye.");
		}
	  }
		
		
	}
	}
}
