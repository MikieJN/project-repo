package com.example.screen;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

import com.example.dao.Proj0DBConnection;

public class FirstScreen {
	
	private Proj0DBConnection pCon;
	
	public FirstScreen(){
		
	}
	
	public FirstScreen(Proj0DBConnection pCon){
		this.pCon = pCon;
	}
	
	public void CreateFirstScreen(Proj0DBConnection pCon) {
		
		try(Connection con = pCon.getDBConnection()) {
		System.out.println("Welcome to the Project-0 Bank!");
		System.out.println("Select an option below to continue.");
		
		System.out.println("[0] Create a new customer account");
		System.out.println("[1] Login to an existing user account");
		System.out.println("[2] Login to an employee account");
		System.out.println("[3] Login to an existing admin account");
		System.out.println("[9] Quit the program");
		
		Scanner sc = new Scanner(System.in);
		int x = sc.nextInt();
		
		if(x == 0) {
			
			CreateCustomerScreen ccs = new CreateCustomerScreen(pCon);
			ccs.CreateCustomer(pCon);
			
		} else if (x == 1) {
			
			LoginScreen ls = new LoginScreen(pCon);
			ls.LoginCustomer();
		
		} else if (x == 2) {
			
			EmployeeLogin el = new EmployeeLogin(pCon);
			el.LoginEmployee();
			
		} else if (x == 3) {
			
			AdminLogin al = new AdminLogin(pCon);
			al.LoginAdmin();
			
		} else if (x == 4) {
			
		} else if (x == 9) {
			System.out.println("Good bye!");
			return;
		}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
	}

}
