package com.example.screen;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.example.dao.AccountDaoImp;
import com.example.dao.Proj0DBConnection;
import com.example.dao.UserDaoImp;
import com.example.model.Account;
import com.example.model.User;


public class AdminLogin {
	
private Proj0DBConnection pCon = new Proj0DBConnection();
	
	public AdminLogin() {
		
	}
	
	public AdminLogin(Proj0DBConnection pCon) {
		this.pCon = pCon;
	}
	
	public void LoginAdmin() {
		String fname;
		String lname;
		String password;
		String userType = "A";
		
		
		int choice;
		double dchoice;
		double wchoice;
		double tchoice;
		
		User u = new User();
		Account a = new Account();
		List<User> user = new ArrayList<>();
		List<Account> acct = new ArrayList<>();
		
		String accountType;
		double accountFunds;
		String accountStatus;
		int accountNum;
		
		String sql;
		
		UserDaoImp uDao = new UserDaoImp(pCon);
		AccountDaoImp aDao = new AccountDaoImp(pCon);

System.out.println("You are in the admin login page.");
System.out.println("Enter your first name: ");
		
		Scanner sc = new Scanner(System.in);
		
		fname = sc.nextLine();
		
		
		System.out.println("Enter your last name: ");
		
		lname = sc.nextLine();
		
		
		System.out.println("Enter your password: ");
		
		password = sc.nextLine();
		
		u = uDao.getUserByLogin(fname, lname, password);
		
		if(!u.getUserType().equals("A")) {
			System.out.println(u.getUserType());
			System.out.println("These credentials are not listed for this type account.");
		} else {
		
		//if(u.equals(u2 = uDao.getUserNoID(fname, lname, password, userType)) == true) {
		
		System.out.println("Welcome " + fname + " you are signed in.");
		System.out.println(u.toString());
		
		System.out.println("Displaying all customer accounts.");
		try(Connection con = pCon.getDBConnection()){
			
			String sql1 = "select * from accounts ac left outer join users_junction_accounts uja on ac.account_num = uja.a_num \r\n"
					+ "left outer join users u on uja.u_id = u.user_id";
			
			PreparedStatement ps = con.prepareStatement(sql1);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				acct.add(new Account(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getDouble(4)));
				user.add(new User(rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getInt(11)));
			}
			
			for(int i = 0; i < acct.size(); i++) {
				System.out.println(acct.get(i).toString() + user.get(i).toString());
				
			}
			

			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		System.out.println("Select an option below to view or edit an account.");
		System.out.println("[0] Deposit");
		System.out.println("[1] Withdraw");
		System.out.println("[2] Transfer");
		System.out.println("[3] Approve/Deny account");
		System.out.println("[4] Cancel an account");
		System.out.println("[9] Logout and quit");
		
		choice = sc.nextInt();
		
		if(choice == 0) {
			System.out.println("You have selected Deposit. Please enter the amount you wish to deposit.");
			dchoice = sc.nextDouble();
			System.out.println("Please select the account number you would like to deposit $" + dchoice + " into.");
			accountNum = sc.nextInt();
			a = aDao.getAcctByNum(accountNum);
			accountFunds = a.getAccountFunds();
			aDao.deposit(dchoice, accountFunds, accountNum);
		}
		
		else if(choice == 1) {
			System.out.println("You have selected Withdraw. Please enter the amount you wish to withdraw.");
			wchoice = sc.nextDouble();
			System.out.println("Please select the account number you would like to withdraw $" + wchoice + " from.");
			accountNum = sc.nextInt();
			a = aDao.getAcctByNum(accountNum);
			accountFunds = a.getAccountFunds();
			aDao.withdraw(wchoice, accountFunds, accountNum);
		}
		
		else if(choice == 2) {
			System.out.println("You have selected Transfer. Please enter the amount you wish to transfer.");
			tchoice = sc.nextDouble();
			System.out.println("Please select the account number you would like to transfer $" + tchoice + " from.");
			choice = sc.nextInt();
			System.out.println("Please select the account number you would like to transfer into.");
			int choice2 = sc.nextInt();
			aDao.transfer(choice, choice2 ,tchoice);
		}
		
		else if(choice == 3) {
			System.out.println("You have chosen to approve/deny open account applications.");
			acct = aDao.getAcctByStatus();
			for(int i = 0; i < acct.size(); i++) {
				System.out.println(acct.get(i).toString());
			}
			System.out.println("Select the account number you would like to approve or deny");
			accountNum = sc.nextInt();
			if(choice > 0) {
				System.out.println("You have chosen account number: " + accountNum);
				System.out.println("Would you like to approve or deny account number " + accountNum + "?");
				System.out.println("[0] Approve");
				System.out.println("[1] Deny");
				choice = sc.nextInt();
				if(choice == 0) {
					a = aDao.getAcctByNum(accountNum);
					aDao.updateAcctStatus("A", accountNum);
					System.out.println("You have approved the account");
				} else if(choice == 1) {
					a = aDao.getAcctByNum(accountNum);
					aDao.updateAcctStatus("D", accountNum);
					System.out.println("You have denied the account");
				}
				
			}
		}
		
		else if(choice == 4) {
			System.out.println("You have chosen to cancel an account. (Warning: Cancelling an account will delete it from the database.)");
			acct = aDao.getAllAccounts();
			for(int i = 0; i < acct.size(); i++) {
				System.out.println(acct.get(i).toString());
			}
			System.out.println("Input the account number you would like to cancel.");
			accountNum = sc.nextInt();
			aDao.delete(accountNum);
			System.out.println("You have cancelled account number: " + accountNum);
		}
		
		else if(choice == 9) {
			System.out.println("You have been logged out! Goodbye.");
			sc.close();
		}
		//} else {
			//System.out.println("There was an error in your login information.");
		//}
		
	}
  }

}
