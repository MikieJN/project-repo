package com.example.screen;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.example.dao.AccountDaoImp;
import com.example.dao.Proj0DBConnection;
import com.example.dao.UserDaoImp;
import com.example.model.Account;
import com.example.model.User;


public class EmployeeLogin {

private Proj0DBConnection pCon = new Proj0DBConnection();
	
	public EmployeeLogin() {
		
	}
	
	public EmployeeLogin(Proj0DBConnection pCon) {
		this.pCon = pCon;
	}
	
	public void LoginEmployee() {
		String fname;
		String lname;
		String password;
		String userType = "E";
		
		
		int choice;
		String echoice;
		
		User u = new User();
		Account a = new Account();
		
		String accountType;
		double accountFunds;
		String accountStatus;
		int accountNum;
		List<Account> acct = new ArrayList<>();
		List<User> user = new ArrayList<>();
		
		UserDaoImp uDao = new UserDaoImp(pCon);
		AccountDaoImp aDao = new AccountDaoImp(pCon);

System.out.println("You are in the employee login page.");
System.out.println("Enter your first name: ");
		
		Scanner sc = new Scanner(System.in);
		
		fname = sc.nextLine();
		
		
		System.out.println("Enter your last name: ");
		
		lname = sc.nextLine();
		
		
		System.out.println("Enter your password: ");
		
		password = sc.nextLine();
		
		u = uDao.getUserByLogin(fname, lname, password);
		
		
		if(!u.getUserType().equals("E")) {
			System.out.println("These credentials are not listed for this type account.");
		} else {
		//if(u.equals(u2 = uDao.getUserNoID(fname, lname, password, userType)) == true) {
		
		System.out.println("Welcome " + fname + " you are signed in.");
		System.out.println(u.toString());
		
		System.out.println("Select an option below to view or edit customer account(s).");
		System.out.println("[0] View all customers and accounts");
		System.out.println("[1] Approve/Deny account applications");
		System.out.println("[2] Logout and quit");
		
		choice = sc.nextInt();
		
		if(choice == 0) {
			System.out.println("You have chosen to view customers and their accounts.");
			try(Connection con = pCon.getDBConnection()){
				
				String sql = "select * from accounts ac left outer join users_junction_accounts uja on ac.account_num = uja.a_num \r\n"
						+ "left outer join users u on uja.u_id = u.user_id where user_type = 'C'";
				
				PreparedStatement ps = con.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				
				while(rs.next()) {
					acct.add(new Account(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getDouble(4)));
					user.add(new User(rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getInt(11)));
				}
				
				for(int i = 0; i < acct.size(); i++) {
					System.out.println(acct.get(i).toString() + user.get(i).toString());
					
				}
				

				
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		else if(choice == 1) {
			System.out.println("You have chosen to approve/deny open account applications.");
			acct = aDao.getAcctByStatus();
			for(int i = 0; i < acct.size(); i++) {
				System.out.println(acct.get(i).toString());
			}
			System.out.println("Select the account number you would like to approve or deny");
			accountNum = sc.nextInt();
			if(choice > 0) {
				System.out.println("You have chosen account number: " + choice);
				System.out.println("Would you like to approve or deny account number " + choice + "?");
				System.out.println("[0] Approve");
				System.out.println("[1] Deny");
				choice = sc.nextInt();
				if(choice == 0) {
					a = aDao.getAcctByNum(accountNum);
					aDao.updateAcctStatus("A", accountNum);
					System.out.println("You have approved the account");
				} else if(choice == 1) {
					a = aDao.getAcctByNum(accountNum);
					aDao.updateAcctStatus("D", accountNum);
					System.out.println("You have denied the account");
				}
				
			}
			
		}
		
		else if(choice == 2) {
			System.out.println("You have been logged out. Goodbye!");
			sc.close();
		}
	  }
	}

}
