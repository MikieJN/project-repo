package com.example.screen;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.example.dao.AccountDaoImp;
import com.example.dao.Proj0DBConnection;
import com.example.dao.UserDaoImp;
import com.example.model.Account;
import com.example.model.User;

public class CreateCustomerScreen {
	
	private Proj0DBConnection pCon;
	
	public CreateCustomerScreen() {
	}
	
	public CreateCustomerScreen(Proj0DBConnection pCon) {
		this.pCon = pCon;
	}
	
	public void CreateCustomer(Proj0DBConnection pCon) {
		System.out.println("Creating a new customer account.");
		
		String fname;
		String lname;
		String password;
		String userType = "C";
		int userID;
		
		Account a = new Account();
		User u = new User();
		List <User> user = new ArrayList<>();
		List <Account> acct = new ArrayList<>();
		
		String accountType;
		double accountFunds;
		String accountStatus = "P";
		int accountNum;
		
		String sql;
		
		UserDaoImp uDao = new UserDaoImp(pCon);
		
		System.out.println("Enter your first name: ");
		
		Scanner sc = new Scanner(System.in);
		
		fname = sc.nextLine();
		
		System.out.println("Enter your last name: ");
		
		lname = sc.nextLine();
		
		System.out.println("Enter your password: ");
		
		password = sc.nextLine();
		
		uDao.insertUser(fname, lname, password, userType);
		
		u = uDao.getUserByLogin(fname, lname, password);
		
		userID = u.getUserID();
		
		System.out.println("What kind of account would you like to apply for " + fname + "? (Type 'C' for Checking or 'S' for Savings");
		
		AccountDaoImp aDao = new AccountDaoImp(pCon);
		
		accountType = sc.nextLine();
		if(accountType.equalsIgnoreCase("c") == true) {
			System.out.println("You're applying for a checking account, how much money would you like to start the account with? (Valid entries are numbers with decimals.)");
			accountFunds = sc.nextDouble();
			if(accountFunds > 0) {
				aDao.insertAccount("Checking", accountStatus, accountFunds);
				
				System.out.println("Your checking account with $" + accountFunds + " is pending approval!");
				sc.close();
				
			} else {
				while(accountFunds <= 0) {
					System.out.println("Invalid entry, please enter an amount that is larger than 0 using numbers with decimals.");
					accountFunds = sc.nextDouble();
				}
				System.out.println("Your checking account with $" + accountFunds + " is pending approval!");
				aDao.callableInsertAccount("Checking", accountStatus, accountFunds);
				
				
				sc.close();
				
			}
			
			
			
		} else if(accountType.equalsIgnoreCase("s") == true) {
			System.out.println("You're applying for a savings account, how much money would you like to start the account with? (Valid entries are numbers with decimals.)");
			accountFunds = sc.nextDouble();
			
		if(accountFunds > 0) {
			System.out.println("Your savings account with $" + accountFunds + " is pending approval!");
			aDao.callableInsertAccount("Saving", accountStatus, accountFunds);
			sc.close();
			
		} else {
			while(accountFunds <= 0) {
				System.out.println("Invalid entry, please enter an amount that is larger than 0 using numbers with decimals.");
				accountFunds = sc.nextDouble();
			}
			System.out.println("Your savings account with $" + accountFunds + " is pending approval!");
			aDao.callableInsertAccount("Saving", accountStatus, accountFunds);
			sc.close();
			
			
		}
	  }
		
		
	}
	}

