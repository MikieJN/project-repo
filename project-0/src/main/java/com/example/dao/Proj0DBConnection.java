package com.example.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Proj0DBConnection {
	
	ClassLoader classLoader = getClass().getClassLoader();
	InputStream is;
	Properties p = new Properties();
	
	public Proj0DBConnection() {
		is = classLoader.getResourceAsStream("connection.properties");
		try {
			p.load(is);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Connection getDBConnection() throws SQLException{
	//	final String URL = p.getProperty("URL");
		//final String username = p.getProperty("username");
		//final String password = p.getProperty("password");
		
		final String URL = "jdbc:postgresql://revdb-2109.cixlhgine7gs.us-east-2.rds.amazonaws.com:5432/project0";
		final String username = "bankadmin";
		final String password = "adm1nPW";
		return DriverManager.getConnection(URL, username, password);
	}

}
