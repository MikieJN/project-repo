package com.example.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.example.model.Account;
import com.example.model.User;

public class AccountDaoImp implements AccountDao {
	
private Proj0DBConnection pCon = new Proj0DBConnection();
	
	
	public AccountDaoImp() {
		
	}
	
	public AccountDaoImp(Proj0DBConnection pCon) {
		this.pCon = pCon;
	}

	
	public void insertAccount(String accountType, String accountStatus, double accountFunds) {
		try(Connection con = pCon.getDBConnection()){
			String sql = "insert into accounts(account_type, account_status, account_funds) values('" + accountType + "','" + accountStatus + "','" + accountFunds + "')";
			
			Statement statement = con.createStatement();
			int changed = statement.executeUpdate(sql);
			System.out.println("Number of rows changed: " + changed);
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
	}

	
	public void preparedInsertAccount(String accountType, String accountStatus, double accountFunds) {
		try(Connection con = pCon.getDBConnection()){
			String sql = "insert into accounts(account_type, account_status, account_funds) values(?,?,?)";
			PreparedStatement prepared = con.prepareStatement(sql);
			prepared.setString(1,accountType);
			prepared.setString(2, accountStatus);
			prepared.setDouble(3, accountFunds);
			
			int changed = prepared.executeUpdate(sql);
			System.out.println("Number of rows changed: " + changed);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	
	public void callableInsertAccount(String accountType, String accountStatus, double accountFunds) {
		
			try(Connection con = pCon.getDBConnection()){
			
			String sql = "{? = call insert_account(?,?,?)}";
			
			CallableStatement cs = con.prepareCall(sql);
			
			cs.registerOutParameter(1, Types.VARCHAR);
			cs.setString(2, accountType);
			cs.setString(3, accountStatus);
			cs.setDouble(4, accountFunds);
			
			cs.execute();
			
			System.out.println("The status of this account is currently: " + cs.getString(3));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public double getAcctFunds(int accountNum) {
		try(Connection con = pCon.getDBConnection()){
			String sql = "select account_funds from accounts where account_num = ?";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			Account acct = new Account();
			
			double funds = rs.getDouble(1);
			acct.setAccountFunds(funds);
			
			return funds;
		}catch(SQLException e) {
			
			e.printStackTrace();
			
		}
		return 0;
	}
	
	public Account getAcctByNum(int accountNum) {
		try(Connection con = pCon.getDBConnection()){
			String sql = "select * from accounts where account_num = ?";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, accountNum);
			ResultSet rs = ps.executeQuery();
			
			Account acct = new Account();
			
			while(rs.next()) {
			acct = new Account(rs.getString(1), rs.getString(2), rs.getDouble(4), rs.getInt(3));
			}
			
			return acct;
		}catch(SQLException e) {
			
			e.printStackTrace();
			
		}
		return null;
	}
	
	public List<Account> getAcctByUserID(int userID) {
		List <Account> acct = new ArrayList<>();
		User u = new User();
		u.setUserID(userID);
		
		try(Connection con = pCon.getDBConnection()){
			String sql = "select * from accounts ac left outer join users_junction_accounts uja on ac.account_num = uja.a_num \r\n"
					+ "left outer join users u on uja.u_id = u.user_id where u.user_id = ?;";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, userID);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				acct.add(new Account(rs.getString(1), rs.getString(2), rs.getDouble(4), rs.getInt(3)));
			}
			
			
			
			return acct;
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
			
		}
	
	public List<Account> getAcctByStatus() {
		List <Account> acct = new ArrayList<>();
		
		try(Connection con = pCon.getDBConnection()){
			String sql = "select * from accounts ac left outer join users_junction_accounts uja on ac.account_num = uja.a_num left outer join users u on uja.u_id = u.user_id where ac.account_status = 'P'";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			System.out.println(rs);
			
			while(rs.next()) {
				acct.add(new Account(rs.getString(1), rs.getString(2), rs.getDouble(4), rs.getInt(3)));
			}
			
			return acct;
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
			
		}



	@Override
	public List<Account> getAllAccounts() {
		
			List<Account> acctList = new ArrayList<>();
		
			try(Connection con = pCon.getDBConnection()){
			
			String sql = "select * from accounts";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				acctList.add(new Account(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getDouble(4)));
			}
			
			return acctList;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		return null;
	}
	
	public void delete(int accountNum) {
		try(Connection con = pCon.getDBConnection()){
	
		String sql = "delete from accounts where account_num = ?";
		
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setInt(1, accountNum);
		
		int changed = ps.executeUpdate();
		System.out.println("Number of rows changed: " + changed);
		System.out.println("The row with the account number " + accountNum + " has been deleted.");
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public void update(String accountType, String accountStatus, double accountFunds, int accountNum) {
		try(Connection con = pCon.getDBConnection()){
			
		String sql = "update accounts set account_type = ?, account_status = ?, account_funds = ? where account_num = ?";
		
		PreparedStatement ps = con.prepareStatement(sql);
		int changed = ps.executeUpdate();
		
		System.out.println("Number of rows changed: " + changed);

	} catch (SQLException e) {
		e.printStackTrace();
	}

  }
	
	
	public void updateAcctStatus(String accountStatus, int accountNum) {
		try(Connection con = pCon.getDBConnection()){
			
		String sql = "update accounts set account_status = ? where account_num = ?";
		
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, accountStatus);
		ps.setInt(2, accountNum);
		
		int changed = ps.executeUpdate();
		
		System.out.println("Number of rows changed: " + changed + ". Account number " + accountNum + " has been changed to " + accountStatus + ".");

	} catch (SQLException e) {
		e.printStackTrace();
	}

  }
	
	public void deposit(double deposit, double accountFunds, int accountNum) {
		
		if(deposit <= 0) {
			Scanner sc = new Scanner(System.in);
			while(deposit <= 0) {
			System.out.println("You can only deposit amounts greater than zero. Try again!");
			sc.nextDouble();
			deposit = sc.nextDouble();
			}
			sc.close();
		}
		
		try(Connection con = pCon.getDBConnection()){
			
		String sql = "update accounts set account_funds = ?+? where account_num = ?";
		PreparedStatement ps = con.prepareStatement(sql);
		
		ps.setDouble(1, accountFunds);
		ps.setDouble(2, deposit);
		ps.setInt(3, accountNum);
		
		accountFunds = accountFunds + deposit;
		
		int changed = ps.executeUpdate();
		changed = ps.getUpdateCount();
		
		System.out.println("Number of rows changed: " + changed + ". You successfully deposited $" + deposit + " Your new total in account_funds is $" + accountFunds + ".");
		
		ps.close();
	} catch(SQLException e) {
		e.printStackTrace();
		}
	}
	
	public void withdraw(double withdraw, double accountFunds, int accountNum) {
		if(withdraw > accountFunds || withdraw <= 0) {
			System.out.println("You cannot withdraw more than what is in the account and the withdraw amount must be greater than zero.");
			return;
		}
		try(Connection con = pCon.getDBConnection()){
			
		String sql = "update accounts set account_funds = account_funds-? where account_num = ?";
		PreparedStatement ps = con.prepareStatement(sql);
		
		ps.setDouble(1, withdraw);
		ps.setInt(2, accountNum);
		
		ps.execute();
		
		accountFunds = accountFunds - withdraw;
		
		System.out.println("You successfully withdrew $" + withdraw + ". Your new total in account_funds is $" + accountFunds +".");
	} catch(SQLException e) {
		e.printStackTrace();
	}
	}
	
	public void transfer(int accountNum1, int accountNum2, double transfer) {
		if(transfer <= 0 ) {
			System.out.println("You must transfer a number greater than zero.");
			return;
		}
		try(Connection con = pCon.getDBConnection()){
			
		String sql1 = "update accounts set account_funds = (account_funds-?) where account_num = ?";
		String sql2 = "update accounts set account_funds = (account_funds+?) where account_num = ?";
		
		PreparedStatement ps1 = con.prepareStatement(sql1);
		PreparedStatement ps2 = con.prepareStatement(sql2);
		
		ps1.setDouble(1, transfer);
		ps1.setInt(2, accountNum1);
		
		ps2.setDouble(1, transfer);
		ps2.setInt(2, accountNum2);
		
		int c1 = ps1.executeUpdate();
		int c2 = ps2.executeUpdate();
		
		System.out.println("Number of rows changed: " + (c1+c2) + ". You successfully transfered $" + transfer + " from account number: " + accountNum1 + " to account number: " + accountNum2 + ".");
		return;
		
		
	} catch(SQLException e) {
		e.printStackTrace();
	}
	}


}
