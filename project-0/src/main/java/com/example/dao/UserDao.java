package com.example.dao;


import java.util.List;

import com.example.model.User;

//Create Dao layer object to be implemented.

public interface UserDao {
	
	void insertUser(String fname, String lname, String password, String userType);
	
	void preparedInsertUser(String fname, String lname, String password, String userType);
	
	void callableInsertUser(String fname, String lname, String password, String userType);
	
	void insertUserObj(User user);
	
	List<User> getAllUsers();

}
