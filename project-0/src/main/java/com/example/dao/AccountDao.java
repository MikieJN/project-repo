package com.example.dao;

import java.util.List;

import com.example.model.Account;

public interface AccountDao {
	
	void insertAccount(String accountType, String accountStatus, double accountFunds);
	
	void preparedInsertAccount(String accountType, String accountStatus, double accountFunds);
	
	void callableInsertAccount(String accountType, String accountStatus, double accountFunds);
	
	
	List<Account> getAllAccounts();

}
