package com.example.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.example.model.Account;
import com.example.model.User;

//Implement Dao layer object.

public class UserDaoImp implements UserDao {
	
	private Proj0DBConnection pCon = new Proj0DBConnection();
	
	
	public UserDaoImp() {
		
	}
	
	public UserDaoImp(Proj0DBConnection pCon) {
		this.pCon = pCon;
	}
	
	
	@Override
	public void insertUser(String fname, String lname, String password, String userType) {
		
		try(Connection con = pCon.getDBConnection()){
			String sql = "insert into users(First_Name, Last_Name, pw, user_type) values('" + fname + "','" + lname + "','" + password + "','" + userType + "')";
			
			Statement statement = con.createStatement();
			int changed = statement.executeUpdate(sql);
			System.out.println("Number of rows changed: " + changed);
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	

	@Override
	public void preparedInsertUser(String fname, String lname, String password, String userType) {
		// TODO Auto-generated method stub
		
		//Insert user using prepared statement to combat possible SQL injection.
		
		try(Connection con = pCon.getDBConnection()){
			String sql = "insert into users(First_Name, Last_Name, pw, account_type, account_funds) values(?,?,?,?)";
			PreparedStatement prepared = con.prepareStatement(sql);
			prepared.setString(1,fname);
			prepared.setString(2, lname);
			prepared.setString(3, password);
			prepared.setString(4, userType);
			
			int changed = prepared.executeUpdate(sql);
			System.out.println("Number of rows changed: " + changed);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void insertUserObj(User user) {
		// TODO Auto-generated method stub
		
		
	}
	
	public User getUserByLogin(String fname, String lname, String password) {
		try(Connection con = pCon.getDBConnection()){
			String sql = "select * from users where first_name = ? and last_name = ? and pw = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, fname);
			ps.setString(2, lname);
			ps.setString(3, password);
			
			ResultSet rs = ps.executeQuery();
			
			User user = new User();
			while(rs.next()) {
			user = new User(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5));
			
			}
			
			return user;
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public User getUserByID(Integer userID) {
		
		try(Connection con = pCon.getDBConnection()){
			String sql = "select * from users u left outer join users_junction_accounts uja on u.user_id = uja.u_id \r\n"
					+ "left outer join accounts ac on uja.a_num = ac.account_num where u.user_id = ?";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, userID);
			ResultSet rs = ps.executeQuery();
			
			User user = new User();
			
			while(rs.next()) {
				user = new User(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), Integer.valueOf(rs.getInt(5)));
				user.setFname(rs.getString(1));
			}
			
			user.setUserID(userID);
			
			return user;
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
public User getUserNoID(String fname, String lname, String password, String userType) {
	
	Integer userID = 0;
		
		try(Connection con = pCon.getDBConnection()){
			String sql = "select * from users u left outer join users_junction_accounts uja on u.user_id = uja.u_id \r\n"
					+ "left outer join accounts ac on uja.a_num = ac.account_num where u.First_Name = ? and u.Last_Name = ? and u.pw = ? and u.user_type = ?";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, fname);
			ps.setString(2, lname);
			ps.setString(3, password);
			ps.setString(4, userType);
			ps.setInt(5, Integer.valueOf(userID));
			ResultSet rs = ps.executeQuery();
			
			User user = new User();
			Account acct = new Account();
			
			while(rs.next()) {
				user = new User(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), Integer.valueOf(rs.getInt(5)));
				
				acct = new Account(rs.getString(6), rs.getString(7), rs.getDouble(9), rs.getInt(8));
				
				acct.setAccountType(rs.getString(6));
				acct.setAccountStatus(rs.getString(7));
				acct.setAccountNum(rs.getInt(8));
				acct.setAccountFunds(rs.getDouble(9));
			}
			
			user.setLname(lname);
			user.setPassword(password);
			user.setUserType(userType);
			
			
			return user;
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	

	@Override
	public List<User> getAllUsers() {
		// TODO Auto-generated method stub
		
		List<User> userList = new ArrayList<>();
		
		try(Connection con = pCon.getDBConnection()){
			
			String sql = "select * from users";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				userList.add(new User(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5)));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return userList;
	}


	@Override
	public void callableInsertUser(String fname, String lname, String password, String userType) {
		// TODO Auto-generated method stub
		
		try(Connection con = pCon.getDBConnection()){
			
			String sql = "{? = call insert user(?,?,?,?,?,?)}";
			
			CallableStatement cs = con.prepareCall(sql);
			
			cs.registerOutParameter(1, Types.VARCHAR);
			cs.setString(2, fname);
			cs.setString(3, lname);
			cs.setString(4, password);
			cs.setString(5, userType);
			cs.execute();
			
			System.out.println(cs.getString(1));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}


}
