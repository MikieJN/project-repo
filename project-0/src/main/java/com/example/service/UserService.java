package com.example.service;

import java.util.InputMismatchException;

import com.example.dao.UserDaoImp;
import com.example.model.User;
import com.sun.tools.javac.parser.Scanner;

public class UserService {
	
	private UserDaoImp uDao;
	
	public UserService() {
		
	}
	
	public UserService(UserDaoImp uDao) {
		this.uDao = uDao;
	}
	
	public User getUserByID(Integer userID) {
		
		User user = uDao.getUserByID(userID);
		if(user.getFname() ==  null) {
			throw new NullPointerException("There is no user with the user ID: " + userID);
		}
		
		return user;
	}

  }

