package com.example.model;

public class Account {
	
	private int accountNum;
	private String accountType;
	private String accountStatus;
	private double accountFunds;
	
	public Account() {
		
	}
	
	
	public Account(String accountType, String accountStatus, double accountFunds) {
		this.accountType = accountType;
		this.accountStatus = accountStatus;
		this.accountFunds = accountFunds;
	}
	
	public Account(String accountType, String accountStatus) {
		this.accountType = accountType;
		this.accountStatus = accountStatus;
	}
	
	public Account(String accountType, String accountStatus, double accountFunds, int accountNum) {
		this.accountNum = accountNum;
		this.accountType = accountType;
		this.accountStatus = accountStatus;
		this.accountFunds = accountFunds;
	}
	
	public Account(String accountType, String accountStatus, int accountNum, double accountFunds) {
		this.accountNum = accountNum;
		this.accountType = accountType;
		this.accountStatus = accountStatus;
		this.accountFunds = accountFunds;
	}
	
	public Account(double accountFunds, int accountNum) {
		this.accountFunds = accountFunds;
		this.accountNum = accountNum;
	}
	
	public Account(double accountFunds) {
		this.accountFunds = accountFunds;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public double getAccountFunds() {
		return accountFunds;
	}

	public void setAccountFunds(double accountFunds) {
		this.accountFunds = accountFunds;
	}
	
	@Override
	public String toString() {
		return "Account [Account Type = " + accountType + ", Account Status = " + accountStatus + ", Account Number = " + accountNum + ", Account Funds = " + accountFunds + "]";
	}

	public int getAccountNum() {
		return accountNum;
	}

	public void setAccountNum(int accountNum) {
		this.accountNum = accountNum;
	}


}
