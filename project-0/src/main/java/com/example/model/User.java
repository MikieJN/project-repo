package com.example.model;


public class User {
	
	private Integer userID; //Hold user ID
	private String fname; //Hold first name of the user
	private String lname; //Hold last name of the user
	private String password; //Hold password of user
	private String userType; //Hold user type
	
	//Define constructors for User objects.
	
	public User() {
		
	}
	
	public User(String fname, String lname, String password, String userType, Integer userID) {
		this.userID = userID;
		this.fname = fname;
		this.lname = lname;
		this.password = password;
		this.userType = userType;
	}
		
	public User(String fname, String lname, String password, String userType) {
			this.fname = fname;
			this.lname = lname;
			this.password = password;
			this.userType = userType;
	}
	
	public User(String fname, String lname, String password) {
		this.fname = fname;
		this.lname = lname;
		this.password = password;
}
	
	public User(String fname, String lname) {
		this.fname = fname;
		this.lname = lname;
	}
	
	public User(String fname) {
		this.fname = fname;
	}
	
	public User(int userID) {
		this.userID = userID;
	}
	
	
	//Define methods to access the private variables for User.
	
	public String getFname() {
		return fname;
	}
	
	public void setFname(String name) {
		this.fname = name;
	}
	
	public String getLname() {
		return lname;
	}
	
	public void setLname(String name) {
		this.lname = name;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String newPassword) {
		this.password = newPassword;
	}
	
	public String getUserType() {
		return userType;
	}
	
	public void setUserType(String ut){
		this.userType = ut;
	}
	
	public Integer getUserID() {
		return userID;
	}
	
	public void setUserID(int i) {
		this.userID = i;
	}
	
	//Create toString method for user.
	
	@Override
	public String toString() {
		return "User [First Name = " + fname + ", Last Name = " + lname + ", Password = " + password + ", User Type = " + userType + ", User ID = " + userID + "]";
	}

	

	
	
	//The methods have been defined to access the private User variables.
	//Next implement the methods and variables for the bank database.

}
