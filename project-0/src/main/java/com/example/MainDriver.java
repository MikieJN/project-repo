package com.example;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.example.dao.Proj0DBConnection;
import com.example.dao.UserDaoImp;
import com.example.model.Account;
import com.example.model.User;
import com.example.screen.FirstScreen;
import com.example.screen.LoginScreen;
import com.example.service.UserService;

public class MainDriver {
	
	public static Proj0DBConnection pCon = new Proj0DBConnection();
	
//	public static String askForInput() {
//		int i = 0;
//		for(i = 0; i <= 2; i++) {
//		Scanner sc = new Scanner(System.in);
//		System.out.println("Enter a user first name, last name, and password.");
//		return sc.next();
//		
//		}
//		return null;
//	}
	
	public MainDriver() {
		
	}
	
	public MainDriver(Proj0DBConnection pCon) {
		this.pCon = pCon;
	}
	
	public static void main(String[] args) {
		try(Connection con = pCon.getDBConnection()){
		FirstScreen fs = new FirstScreen();
		LoginScreen ls = new LoginScreen();
		fs.CreateFirstScreen(pCon);
		UserDaoImp uDao = new UserDaoImp();
		UserService uServ = new UserService(uDao);
		
		

		
//		try {
//		System.out.println(uDao.getUserNoID(askForInput(), askForInput(), askForInput(), "C"));
//		}catch(NullPointerException e) {
//			e.printStackTrace();
//		}catch(InputMismatchException e) {
//			System.out.println("That was not a valid integer.");
//			e.printStackTrace();
//		}
		
	}catch(SQLException e) {
		e.printStackTrace();
	}
	
		
		
}

}