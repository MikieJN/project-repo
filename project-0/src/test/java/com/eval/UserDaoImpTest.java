package com.eval;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.Proj0DBConnection;
import com.example.dao.UserDaoImp;
import com.example.model.Account;
import com.example.model.User;
import com.example.service.UserService;

public class UserDaoImpTest {
	
	//Mock classes, methods, and variables.
	@Mock
	private UserDaoImp uDao;
	
	@Mock
	private UserService uServ;
	
	@Mock
	private Proj0DBConnection pc;
	
	@Mock 
	private Connection con;
	
	@Mock
	private PreparedStatement ps;
	
	@Mock
	private ResultSet rs;
	
	@Mock
	private User testUser;
	
	@Mock
	private Account testAcct;
	
	@BeforeEach
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
		uDao = new UserDaoImp(pc);
		uServ = new UserService(uDao);
		testUser = new User("Testy", "McTesterson", "TestWord", "T", 66);
		testAcct = new Account("Checking", "A", 620.49, 24);
		when(uDao.getUserByID(66)).thenReturn(testUser);
	}
	
	@Test
	public void testGetUserSuccess() {
		assertEquals(uServ.getUserByID(66), testUser);
	}
	
	@Test
	public void testGetUserFailure() {
		assertThrows(NullPointerException.class, ()->uServ.getUserByID(0));
	}

}
